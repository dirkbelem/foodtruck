/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.foodtruck.bo;

import br.com.foodtruck.dao.DAOUser;
import br.com.foodtruck.fw.Criptografia;
import br.com.foodtruck.fw.Data;
import br.com.foodtruck.fw.Email;
import br.com.foodtruck.to.TOUser;
import java.sql.Connection;
import java.util.Random;

/**
 *
 * @author dirceu
 */
public class BOUser {

    public static void insert(TOUser t) throws Exception {
        try (Connection c = Data.openConnection()) {
            t.setPassword(Criptografia.sha1(t.getPassword()));
            DAOUser.insert(c, t);
        }
    }

    public static void update(TOUser t) throws Exception {
        try (Connection c = Data.openConnection()) {
            DAOUser.update(c, t);
        }
    }

    public static TOUser auth(TOUser t) throws Exception {
        try (Connection c = Data.openConnection()) {

            t.setPassword(Criptografia.sha1(t.getPassword()));

            return DAOUser.auth(c, t);
        }
    }

    public static TOUser forgot(TOUser t) throws Exception {
        try (Connection c = Data.openConnection()) {

            t = DAOUser.forgot(c, t);

            if (t == null) {
                return null;
            } else {

                Random rand = new Random();
                String newPass = String.valueOf(Long.toHexString(rand.nextLong()));

                if (newPass.length() > 6) {
                    newPass = newPass.substring(0, 5);
                }

                String text = "Sua nova senha é: " + newPass;

                t.setPassword(newPass);
                t.setPassword(Criptografia.sha1(t.getPassword()));
                DAOUser.update(c, t);

                Email e = new Email("Nova senha", text, t.getUser());
                e.start();

                return t;
            }

        }
    }

}
