/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.foodtruck.bo;

import br.com.foodtruck.dao.DAOFoodTruck;
import br.com.foodtruck.fw.Data;
import br.com.foodtruck.to.TOFoodTruck;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author dirceubelem
 */
public class BOFoodTruck {

    public static List<TOFoodTruck> find(String name) throws Exception {
        try (Connection c = Data.openConnection()) {
            return DAOFoodTruck.find(c, name);
        }
    }

}
