/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.foodtruck.dao;

import br.com.foodtruck.fw.Data;
import br.com.foodtruck.to.TOUser;
import java.sql.Connection;
import java.sql.ResultSet;

/**
 *
 * @author dirceu
 */
public class DAOUser {

    public static void insert(Connection c, TOUser t) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" insert into user (name, lastname, password, user) values (?, ?, ?, ?) ");

        Data.executeUpdate(c, sql.toString(), t.getName(), t.getLastname(), t.getPassword(), t.getUser());

    }

    public static void update(Connection c, TOUser t) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" update user set name = ?, lastname = ?, user = ?, picture = ?, password = ? where id = ? ");

        Data.executeUpdate(c, sql.toString(), t.getName(), t.getLastname(), t.getUser(), t.getPicture(), t.getPassword(), t.getId());

    }

    public static TOUser auth(Connection c, TOUser t) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" select id, name, lastname, password, user, picture from user ");
        sql.append(" where user = ? and password = ? ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), t.getUser(), t.getPassword())) {

            if (rs.next()) {

                TOUser tc = new TOUser();
                tc.setId(rs.getInt("id"));
                tc.setName(rs.getString("name"));
                tc.setLastname(rs.getString("lastname"));
                tc.setPicture(rs.getString("picture"));
                tc.setUser(rs.getString("user"));

                return tc;
            } else {
                return null;
            }

        }
    }

    public static TOUser get(Connection c, TOUser t) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" select id, name, lastname, password, user, picture from user ");
        sql.append(" where id = ? ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), t.getId())) {

            if (rs.next()) {

                TOUser tc = new TOUser();
                tc.setId(rs.getInt("id"));
                tc.setName(rs.getString("name"));
                tc.setLastname(rs.getString("lastname"));
                tc.setPicture(rs.getString("picture"));
                tc.setUser(rs.getString("user"));

                return tc;
            } else {
                return null;
            }

        }
    }

    public static TOUser forgot(Connection c, TOUser t) throws Exception {

        StringBuilder sql = new StringBuilder();
        sql.append(" select id, name, lastname, password, user, picture from user ");
        sql.append(" where user = ? ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), t.getUser())) {

            if (rs.next()) {

                TOUser tc = new TOUser();
                tc.setId(rs.getInt("id"));
                tc.setName(rs.getString("name"));
                tc.setLastname(rs.getString("lastname"));
                tc.setPicture(rs.getString("picture"));
                tc.setUser(rs.getString("user"));

                return tc;
            } else {
                return null;
            }

        }
    }

}
