/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.foodtruck.dao;

import br.com.foodtruck.fw.Data;
import br.com.foodtruck.to.TOFoodTruck;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dirceubelem
 */
public class DAOFoodTruck {

    public static List<TOFoodTruck> find(Connection c, String name) throws Exception {

        List<TOFoodTruck> l = new ArrayList<>();

        StringBuilder sql = new StringBuilder();
        sql.append(" select id, name, detail, latitude, longitude, picture from foodtruck ");
        sql.append(" where name like ? ");

        try (ResultSet rs = Data.executeQuery(c, sql.toString(), "%" + name + "%")) {

            while (rs.next()) {

                TOFoodTruck t = new TOFoodTruck();
                t.setId(rs.getInt("id"));
                t.setName(rs.getString("name"));
                t.setDetail(rs.getString("detail"));
                t.setLatitude(rs.getDouble("latitude"));
                t.setLongitude(rs.getDouble("longitude"));
                t.setPicture(rs.getString("picture"));

                l.add(t);

            }

        }

        return l;

    }

}
