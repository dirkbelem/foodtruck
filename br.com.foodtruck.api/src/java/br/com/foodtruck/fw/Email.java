/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.foodtruck.fw;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Dirceu
 */
public class Email extends Thread {

    private final String subject;
    private final String message;
    private final String sendTo;

    public Email(String subject, String message, String sendTo) {
        this.subject = subject;
        this.message = message;
        this.sendTo = sendTo;
    }

    @Override
    public void run() {
        try {
            Email.send(subject, message, sendTo);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void send(String subject, String message, String sendTo) throws Exception {
        try {

            final String host = "smtp.gmail.com";
            final String username = "appfindtruck@gmail.com";
            final String password = "Mamamia1";
            final int porta = 587;

            if (!sendTo.trim().equals("")) {

                Properties props = new Properties();
                props.put("mail.smtps.auth", "true");
                props.put("mail.starttls.enable", "true");
                props.put("mail.smtp.port", porta);
//                props.put("mail.from", "midiastats@midiastats.com");
                props.put("mail.debug", "true");

                Session session = Session.getDefaultInstance(props, null);

                MimeMessage msg = new MimeMessage(session);
//                msg.setFrom(new InternetAddress(empresa.getEmailLoja()));
                msg.addRecipient(Message.RecipientType.TO, new InternetAddress(sendTo));
                msg.setSubject(subject, "UTF-8");

                msg.setContent(message, "text/html; charset=ISO-8859-1");

                // set the message content here
                Transport t = session.getTransport("smtps");
                try {
                    t.connect(host, username, password);
                    t.sendMessage(msg, msg.getAllRecipients());
                } finally {
                    t.close();
                }
            } else {
                throw new Exception("Erro ao enviar e-mail");
            }

        } catch (Exception e) {
//            Util.log("EMAIL", "Erro Enviar E-mail: " + e.toString());
        }
    }

}
