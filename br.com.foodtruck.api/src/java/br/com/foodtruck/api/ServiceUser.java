/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.foodtruck.api;

import br.com.foodtruck.bo.BOUser;
import br.com.foodtruck.to.TOUser;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 * REST Web Service
 *
 * @author dirceubelem
 */
@Path("user")
public class ServiceUser {

    @Context
    private UriInfo context;
    @Context
    private HttpServletResponse response;

    @POST
    @Consumes("application/json; charset=utf-8")
    public void insert(TOUser t) throws Exception {
        BOUser.insert(t);
    }

    @POST
    @Path("auth")
    @Consumes("application/json; charset=utf-8")
    @Produces("application/json; charset=utf-8")
    public TOUser auth(TOUser t) throws Exception {
        TOUser u = BOUser.auth(t);
        if (u != null) {
            return u;
        } else {
            response.sendError(403);
            return new TOUser();
        }
    }

    @POST
    @Path("forgot")
    @Consumes("application/json; charset=utf-8")
    public void forgot(TOUser t) throws Exception {
        TOUser u = BOUser.forgot(t);
        if (u == null) {
            response.sendError(404);
        }
    }

}
