/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.foodtruck.api;

import br.com.foodtruck.bo.BOFoodTruck;
import br.com.foodtruck.to.TOFoodTruck;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

/**
 *
 * @author dirceubelem
 */
@Path("foodtruck")
public class ServiceFoodTruck {

    @Context
    private HttpServletResponse response;

    @Context
    private HttpServletRequest request;

    @GET
    @Consumes("application/json; charset=utf-8")
    @Produces("application/json; charset=utf-8")
    public List<TOFoodTruck> find() throws Exception {
        String q = request.getParameter("q");
        return BOFoodTruck.find(q);
    }

}
